document.getElementById("cc").disabled = true;
document.getElementById("dwn-btn").disabled = true;


function reviewFunction() {

    if (document.getElementById("review").onclick) {

        if(nm.value == "") {
            console.log("if is in!");
            $("#nm").addClass("red");
        } else {
            console.log("else is in!");
            $("#nm").addClass("green");
            document.getElementById("name").innerHTML = nm.value;
        }

        if (tl.value == "") {
            document.getElementById("tele").innerHTML = "1300 974 864";

        } else if(tele.innerHTML != "") {

            document.getElementById("tele").innerHTML = tl.value;
        }
        

        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (reg.test(em.value) == false) 
        {
            alert('Invalid Email Address');
            return false;
        }

        if(reg.test(em.value) == true) {
            document.getElementById("email").innerHTML = em.value;
            document.getElementById("position").innerHTML = pos.value;
            document.getElementById("mobile").innerHTML = mb.value
            $(".email-to").attr("href", "mailto:" + em.value);
            document.getElementById("cc").disabled = false;
            document.getElementById("dwn-btn").disabled = false;
        }

        if (document.getElementById("review").onclick) {
            var e = document.getElementById("htmlcode");
            var content = e.innerHTML;
            document.getElementById('text-val').innerHTML = content;
        }
        else {
            document.getElementById('text-val').innerHTML = '';
        }

        return true;

    }
    else {
        alert('Error occurred');
    }
}

// function getFunction() {
//     if (document.getElementById("get").checked) {
//         var e = document.getElementById("htmlcode");
//         var content = e.innerHTML;
//         document.getElementById('text-val').innerHTML = content;
//     }
//     else {
//         document.getElementById('text-val').innerHTML = '';
//     }
// }

function copyCode(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove(); 
}

function download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

// Start file download.
document.getElementById("dwn-btn").addEventListener("click", function(){
    // Download the file as eSign.html file
    var text = document.getElementById("text-val").value;
    var filename = "eSign.html";
    
    download(filename, text);
}, false);